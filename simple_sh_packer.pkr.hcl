
# telling packer to install plugging amazon
# run packer init to get it initiallizes
packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.1"
      source = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "basic-values" {
  ami_name             = "filipe_packer_ami_http_sh_script {{timestamp}}"
  instance_type        = "t2.micro"
  region               = "eu-west-1"
  source_ami           = "ami-05cd35b907b4ffe77"
  vpc_id               = "vpc-4bb64132"
  ssh_username         = "ec2-user"
}

build {
  sources = [
    "source.amazon-ebs.basic-values"
  ]

    provisioner "shell" {
        script = "./httpd_simple_sh.sh"
    }
}