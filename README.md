# Hashicor Packer

A software that uses configuration management languages or software to configure a machine and links up with a cloud provider to deliver an ami. 

Because it links with cloud provider (AWS, azure, GCP) you will need to authenthicate your access. 

There are different ways to do this. 


### Installing software

https://learn.hashicorp.com/tutorials/packer/get-started-install-cli

```bash
sudo yum install -y yum-utils

sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo

sudo yum -y install packer
```


### Authenthication

As mentioned authenthication to cloud provider will be needed. As such we need strategies to key keys and API access code Safe. 

#### Security principles and good practices 

Think about the following things:

1. Can it end up online / open for other to read - bitbucket and github - IT SHOULD NOT BE GIT TRACKED! **DO NOT HAVE THEM in a text file next to your code**
2. If they exist as a file, are they Encrypted? how good is the encryption?
3. If you are giving permisions via ROLES - Then that machine should be very secure - IP locked and SSH to get in. AND secure password for any service you are running

How to exchange secretes and keys?
- Vault that are online - hashicorp vault 
- Sending it manually to your team mate / instructor.
- set them on the environment variables

**using pragamtic API keys**


**setting machine roles**



### Using Packer pkr.hcl language 

1. create a file next your your ansible `httpd_first_packer.pkr.hcl`


Packer has a two sections: 

- *builders* where do you want me to create an AMI? what cloud provider?
- *provisioners* code that configures and provisions a machine - Ansible, bash, chef


### AMIs with Scripts


### AMIs with Ansible


