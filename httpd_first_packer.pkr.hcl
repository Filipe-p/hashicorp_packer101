
# telling packer to install plugging amazon
# run packer init to get it initiallizes
packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.1"
      source = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "basic-values" {
  ami_name             = "filipe_packer_ami {{timestamp}}"
  instance_type        = "t2.micro"
  region               = "eu-west-1"
  source_ami           = "ami-05cd35b907b4ffe77"
  vpc_id               = "vpc-4bb64132"
  ssh_username         = "root"
}

build {
  sources = [
    "source.amazon-ebs.basic-values"
  ]

  provisioner "ansible" {
    playbook_file   = "./not_dragon_httpd_example.yaml"
  }
}